
CREATE DATABASE QLSV;

CREATE TABLE `dmkhoa` (
  `MaKH` varchar(6) DEFAULT NULL,
  `TenKhoa` varchar(30) DEFAULT NULL
);

CREATE TABLE `sinhvien` (
  `MaSV` varchar(6) DEFAULT NULL,
  `HoSV` varchar(30) DEFAULT NULL,
  `TenSV` varchar(15) DEFAULT NULL,
  `NgaySinh` datetime DEFAULT NULL,
  `NoiSinh` varchar(50) DEFAULT NULL,
  `DiaChi` varchar(50) DEFAULT NULL,
  `MaKH` varchar(6) DEFAULT NULL,
  `HocBong` int(11) DEFAULT NULL
)
